# COVID-19 México

Modelados y visualizaciones de la pandemia COVID-19 utilizando base de datos abiertos federal de [SINAVE](https://www.gob.mx/salud/documentos/datos-abiertos-152127).

## Lenguaje de programación

Para este repositorio  se utiliza el lenguaje de programación [R](https://www.r-project.org/) y [RStudio](https://www.rstudio.com/products/rstudio/download/). 

## Contexto COVID-19

Los coronavirus son una amplia familia de virus que pueden causar molestias y daños en el organismo, comenzando con resfriado común hasta daño en los pulmones y neumonía. Estos virus pueden ser causantes del síndrome respiratorio de Oriente Medio (MERS-CoV) y el síndrome respiratorio agudo severo (SRAS).

Actualmente estamos entrando a una nueva cepa de coronavirus que no se había detectado antes en humanos y que está causando numerosas defunciones principalmente en personas mayores de 60 años y con comorbilidades (diabetes, hipertensión arterial, insuficiencia renal, entre otras.)

Todo el mundo ha sido afectado por casusa de la pandemia, tanto a nivel salud física como mental, pérdida de seres queridos y crisis macroeconómica debido a la disminución o paro de actididades buscando mitigar el virus.

![alt text](https://www.municipiocampeche.gob.mx/wp-content/uploads/2020/05/coronavirus.jpg)


## Autor
Diana Paola Montoya Escobar, Doctora en Ciencias en Ingeniería Eléctrica dedicada a la ciencia de datos. 

## Licencia
For open source projects, say how it is licensed.
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

